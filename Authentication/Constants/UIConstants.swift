//
//  UIConstants.swift
//  Authentication
//
//  Created by EreenyEssam on 27/06/2021.
//

import UIKit

struct UIConstants {
    struct LaunchScreen {
        static let loginButtonWidth: CGFloat = 300
        static let loginButtonHeight: CGFloat = 50
        static let loginButtonX: CGFloat = UIScreen.main.bounds.midX - loginButtonWidth/2
        static let loginButtonY: CGFloat = UIScreen.main.bounds.midY - loginButtonHeight/2
        static let loginButtonFrame = CGRect(x: loginButtonX, y: loginButtonY, width: loginButtonWidth, height: loginButtonHeight)
    }

    struct LoginScreen {
        static let loginTextFieldWidth: CGFloat = 300
        static let loginTextFieldHeight: CGFloat = 50
        static let emailTextFieldX: CGFloat = UIScreen.main.bounds.midX - loginTextFieldWidth/2
        static let emailTextFieldY: CGFloat = 100
        static let emailTextFieldFrame = CGRect(x: emailTextFieldX, y: emailTextFieldY, width: loginTextFieldWidth, height: loginTextFieldHeight)
        static let PasswordTextFieldX: CGFloat = UIScreen.main.bounds.midX - loginTextFieldWidth/2
        static let PasswordTextFieldY: CGFloat = loginTextFieldHeight + emailTextFieldY + 20
        static let passwordTextFieldFrame = CGRect(x: PasswordTextFieldX, y: PasswordTextFieldY, width: loginTextFieldWidth, height: loginTextFieldHeight)
        static let loginButtonWidth: CGFloat = 300
        static let loginButtonHeight: CGFloat = 50
        static let loginButtonX: CGFloat = UIScreen.main.bounds.midX - loginButtonWidth/2
        static let loginButtonY: CGFloat = PasswordTextFieldY + loginTextFieldHeight + 20
        static let loginButtonFrame = CGRect(x: loginButtonX, y: loginButtonY, width: loginButtonWidth, height: loginButtonHeight)
    }

    struct MovieScreen {
        static let tableViewFrame = UIScreen.main.bounds
    }

    struct MovieDetailsScreen {

    }
}
