//
//  APIConfig.swift
//  Authentication
//
//  Created by EreenyEssam on 27/06/2021.
//

import Foundation

struct APIConfig {
    struct BaseURL {
        static let url = "https://api.themoviedb.org/3/movie/"
        static let baseImageUrl = "http://image.tmdb.org/t/p/w185"
    }
    struct APIKey {
        static let key = "ab7f35103dac044075517a15da238890"
    }
    struct Endpoints {
        static let recentMovies = BaseURL.url + "now_playing?api_key=" + APIKey.key
    }
}
