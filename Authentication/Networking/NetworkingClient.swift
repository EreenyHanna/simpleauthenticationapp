//
//  NetworkingClient.swift
//  Authentication
//
//  Created by EreenyEssam on 27/06/2021.
//

import Foundation
import Alamofire
import SwiftyJSON

final class NetworkingClient {
    static let sharedInstance = NetworkingClient()
    func execute(_ url: URLConvertible, _ method: HTTPMethod, _ parameters: Parameters? = nil, _ headers: HTTPHeaders? = nil, completion: @escaping(JSON?, Error?) -> Void) {
        AF.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                completion(JSON(value), nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
