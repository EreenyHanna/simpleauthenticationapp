//
//  Button.swift
//  Authentication
//
//  Created by EreenyEssam on 24/06/2021.
//

import Foundation
import UIKit

protocol ButtonDelegate: AnyObject {
    func onTapGestureRecognizer()
}

class Button: UIButton {

    private var tapGestureRecognizer: UITapGestureRecognizer!
    weak var delegate: ButtonDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func prepareViews() {
        prepareButtonView()
        prepareTapGestureOnButtonView()
    }

    private func prepareButtonView() {
        setTitle("Login", for: .normal)
        backgroundColor = .blue
    }

    private func prepareTapGestureOnButtonView() {
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Button.onTapGestureRecognizer(_:)))
        isUserInteractionEnabled = true
        addGestureRecognizer(tapGestureRecognizer)
    }

    func disableBtn() {
        backgroundColor = .gray
        isUserInteractionEnabled = false
    }

    func enableBtn() {
        backgroundColor = .blue
        isUserInteractionEnabled = true
    }

    @objc func onTapGestureRecognizer(_ sender: UITapGestureRecognizer) {
        delegate?.onTapGestureRecognizer()
    }
}
