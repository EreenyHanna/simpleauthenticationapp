//
//  TextField.swift
//  Authentication
//
//  Created by EreenyEssam on 25/06/2021.
//

import Foundation
import UIKit

class TextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func prepareViews() {
       textColor = .black
       borderStyle = .line
    }

    func setSecureTextEntry() {
       isSecureTextEntry = true
    }

    func setKeyboardTypeEmailAddress() {
        keyboardType = .emailAddress
    }
}
