//
//  MovieCell.swift
//  Authentication
//
//  Created by EreenyEssam on 29/06/2021.
//

import Foundation
import UIKit
import Kingfisher

class MovieCell: UITableViewCell {
    let title: UITextView = UITextView()
    let image: UIImageView = UIImageView()
    let rate: MovieRate = MovieRate()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        prepareViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func prepareViews() {
        contentView.addSubview(rate)
        contentView.addSubview(image)
        contentView.addSubview(title)

        image.translatesAutoresizingMaskIntoConstraints = false
        title.translatesAutoresizingMaskIntoConstraints = false
        rate.translatesAutoresizingMaskIntoConstraints = false

        title.isEditable = false
        title.isScrollEnabled = false

        image.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 12).isActive = true
        image.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 12).isActive = true
        image.widthAnchor.constraint(equalToConstant: 40).isActive = true
        image.heightAnchor.constraint(equalToConstant: 30).isActive = true

        title.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 10).isActive = true
        title.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10).isActive = true
        title.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 12).isActive = true
        title.heightAnchor.constraint(equalToConstant: 25).isActive = true

        rate.leadingAnchor.constraint(equalTo: image.trailingAnchor).isActive = true
        rate.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        rate.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 5).isActive = true
        rate.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }

    func setCell(movie: Movie) {
        setImage(movieImage: movie.posterPath)
        setTitle(movieTitle: movie.title)
        rate.setRate(rateData: movie.voteAverage)
    }

    func setImage(movieImage: String) {
        let url = URL(string: movieImage)
        image.kf.setImage(with: url)
    }

    func setTitle(movieTitle: String) {
        title.text = movieTitle
    }
}
