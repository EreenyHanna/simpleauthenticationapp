//
//  Rate.swift
//  Authentication
//
//  Created by EreenyEssam on 01/07/2021.
//

import Foundation
import UIKit

class MovieRate: UIView {
    let rate: UILabel = UILabel()
    let rateIcon: UIImageView = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func prepareViews() {
        addSubview(rate)
        addSubview(rateIcon)

        rate.translatesAutoresizingMaskIntoConstraints = false
        rateIcon.translatesAutoresizingMaskIntoConstraints = false

        rate.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        rate.widthAnchor.constraint(equalToConstant: 30).isActive = true
        rate.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        rateIcon.trailingAnchor.constraint(equalTo: rate.leadingAnchor, constant: -5).isActive = true
        rateIcon.widthAnchor.constraint(equalToConstant: 15).isActive = true
        rateIcon.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        rateIcon.contentMode = .scaleAspectFit
    }

    func setRate(rateData: Double) {
        rate.text = String(rateData)
        rateIcon.image = UIImage(systemName: "star.fill")?.withTintColor(.yellow, renderingMode: .alwaysOriginal)
    }

    func setRateTextColor(color: UIColor) {
        rate.textColor = color
    }
}
