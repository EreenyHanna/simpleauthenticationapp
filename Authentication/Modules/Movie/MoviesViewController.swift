//
//  MoviesController.swift
//  Authentication
//
//  Created by EreenyEssam on 27/06/2021.
//

import Foundation
import UIKit
import SwiftyJSON

class MoviesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let tableView = UITableView(frame: UIConstants.MovieScreen.tableViewFrame)
    var _movies: [Movie]?
    var movies: [Movie]? {
        get { return _movies }
        set {
            _movies = newValue
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTableView()
        getMovies()
    }

    func getMovies() {
        Movies().getMovies { result, error in
            if error == nil {
                self.movies = result
            }
        }
    }

    func prepareTableView() {
        tableView.register(MovieCell.self, forCellReuseIdentifier: "MovieCell")
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as? MovieCell)!
        cell.selectionStyle = .none
        cell.setCell(movie: (movies?[indexPath.row])!)
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies?.count ?? 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movieDetailsViewController = MovieDetailsViewController()
        movieDetailsViewController.movie = movies?[indexPath.row]
        navigationController?.pushViewController(movieDetailsViewController, animated: true)
    }
}
