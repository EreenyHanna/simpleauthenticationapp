//
//  Movie.swift
//  Authentication
//
//  Created by EreenyEssam on 28/06/2021.
//
import Foundation
import SwiftyJSON

class Movie: Codable {
    private var _title: String = ""
    private var _overview: String = ""
    private var _voteAverage: Double = 0.0
    private var _posterPath: String = ""

    init (json: JSON) {
        title = json["title"].string ?? ""
        overview = json["overview"].string ?? ""
        voteAverage = json["vote_average"].double ?? 0.0
        posterPath = json["poster_path"].string ?? ""
    }

    var title: String {
        get { return _title }
        set { _title = newValue }
    }

    var overview: String {
        get { return _overview }
        set { _overview = newValue }
    }

    var posterPath: String {
        get { return _posterPath }
        set { _posterPath = APIConfig.BaseURL.baseImageUrl + newValue }
    }

    var voteAverage: Double {
        get { return _voteAverage }
        set { _voteAverage = newValue }
    }
}
