//
//  MovieDetails.swift
//  Authentication
//
//  Created by EreenyEssam on 29/06/2021.
//

import Foundation
import UIKit
import Kingfisher

class MovieDetailsViewController: UIViewController {

    var movie: Movie?
    let movieImage: UIImageView = UIImageView()
    let movieImageOverlay: UIView = UIView()
    let movieTitle: UILabel = UILabel()
    let movieOverview: UITextView = UITextView()
    let movieRate: MovieRate = MovieRate()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        prepareViews()
        setMovieDetails()
    }

    func prepareViews() {
        movieImageOverlay.addSubview(movieTitle)
        movieImageOverlay.addSubview(movieRate)
        view.addSubview(movieImage)
        view.addSubview(movieImageOverlay)
        view.addSubview(movieOverview)
        prepareMovieImage()
        prepareMovieTitle()
        prepareMovieRate()
        prepareMovieOverView()
    }

    func prepareMovieImage() {
        movieImage.translatesAutoresizingMaskIntoConstraints = false
        movieImageOverlay.translatesAutoresizingMaskIntoConstraints = false

        movieImage.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        movieImage.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        movieImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        movieImage.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.4).isActive = true

        movieImageOverlay.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        movieImageOverlay.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        movieImageOverlay.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        movieImageOverlay.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.4).isActive = true

        movieImageOverlay.backgroundColor = .black
        movieImageOverlay.backgroundColor = movieImageOverlay.backgroundColor!.withAlphaComponent(0.3)
    }

    func prepareMovieTitle() {
        movieTitle.translatesAutoresizingMaskIntoConstraints = false

        movieTitle.leadingAnchor.constraint(equalTo: movieImageOverlay.leadingAnchor, constant: 20).isActive = true
        movieTitle.widthAnchor.constraint(equalTo: movieImageOverlay.widthAnchor, multiplier: 0.7).isActive = true
        movieTitle.bottomAnchor.constraint(equalTo: movieImageOverlay.bottomAnchor, constant: -10).isActive = true
        movieTitle.heightAnchor.constraint(equalTo: movieImageOverlay.heightAnchor, multiplier: 0.2).isActive = true
        movieTitle.textColor = .white
        movieTitle.numberOfLines = .max
        movieTitle.font = UIFont.boldSystemFont(ofSize: 20)
        movieTitle.adjustsFontSizeToFitWidth = true
    }

    func prepareMovieRate() {
        movieRate.translatesAutoresizingMaskIntoConstraints = false
        movieRate.leadingAnchor.constraint(equalTo: movieTitle.trailingAnchor, constant: 20).isActive = true
        movieRate.trailingAnchor.constraint(equalTo: movieImageOverlay.trailingAnchor, constant: -10).isActive = true
        movieRate.bottomAnchor.constraint(equalTo: movieImageOverlay.bottomAnchor, constant: -10).isActive = true
        movieRate.heightAnchor.constraint(equalTo: movieImageOverlay.heightAnchor, multiplier: 0.2).isActive = true

        movieRate.setRateTextColor(color: .white)
    }

    func prepareMovieOverView() {
        movieOverview.translatesAutoresizingMaskIntoConstraints = false

        movieOverview.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        movieOverview.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        movieOverview.topAnchor.constraint(equalTo: movieImage.bottomAnchor, constant: 20).isActive = true
        movieOverview.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        movieOverview.isEditable = false
        movieOverview.isScrollEnabled = false
    }

    func setMovieDetails() {
        let url = URL(string: movie?.posterPath ?? "")
        movieImage.kf.setImage(with: url)
        movieTitle.text = movie?.title
        movieOverview.text = movie?.overview
        movieRate.setRate(rateData: movie?.voteAverage ?? 0.0)
    }
}
