//
//  Movies.swift
//  Authentication
//
//  Created by EreenyEssam on 27/06/2021.
//

import Foundation
import SwiftyJSON

class Movies {
    func getMovies(completion : @escaping([Movie]?, Error?) -> Void) {
        NetworkingClient.sharedInstance.execute(APIConfig.Endpoints.recentMovies, .get) { (result, error) in
            if error == nil {
                var movies = [Movie]()
                if let arrayJSON =  result?["results"].array {
                      movies = arrayJSON.map({return Movie(json: $0)})
                }
                completion(movies, nil)
            } else {
                completion(nil, error)
            }
        }
    }
}
