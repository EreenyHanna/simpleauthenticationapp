//
//  ViewController.swift
//  Authentication
//
//  Created by EreenyEssam on 24/06/2021.
//

import UIKit

class LaunchViewController: UIViewController, ButtonDelegate {
    let loginButton = Button(frame: UIConstants.LaunchScreen.loginButtonFrame)
    let loginViewController = LoginViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        prepareLoginButtonView()
    }

    func prepareLoginButtonView () {
        loginButton.delegate = self
        view.addSubview(loginButton)
    }

    func onTapGestureRecognizer() {
        let navVC = UINavigationController(rootViewController: loginViewController)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: true, completion: nil)
    }
}
