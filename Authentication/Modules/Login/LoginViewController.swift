//
//  LoginViewController.swift
//  Authentication
//
//  Created by EreenyEssam on 24/06/2021.
//

import Foundation
import UIKit

class LoginViewController: UIViewController, ButtonDelegate {

    let emailTextField = TextField(frame: UIConstants.LoginScreen.emailTextFieldFrame)
    let passwordTextField = TextField(frame: UIConstants.LoginScreen.passwordTextFieldFrame)
    let loginButton = Button(frame: UIConstants.LoginScreen.loginButtonFrame)
    let moviesViewController = MoviesViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setBackgroundColor()
        prepareLoginView()
    }

    func setBackgroundColor() {
        view.backgroundColor = .white
    }

    func prepareLoginView() {
        prepareEmailTextField()
        preparePasswordTextField()
        prepareLoginButton()
    }

    func prepareEmailTextField() {
        emailTextField.setKeyboardTypeEmailAddress()
        emailTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(textField:)), for: .editingChanged)
        view.addSubview(emailTextField)
    }

    func preparePasswordTextField() {
        passwordTextField.setSecureTextEntry()
        passwordTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChange(textField:)), for: .editingChanged)
        view.addSubview(passwordTextField)
    }

    func prepareLoginButton() {
        loginButton.delegate = self
        loginButton.disableBtn()
        view.addSubview(loginButton)
    }

    @objc func textFieldDidChange(textField: UITextField) {
        if emailTextField.text!.isEmpty || passwordTextField.text!.isEmpty {
            loginButton.disableBtn()
        } else {
            loginButton.enableBtn()
        }
    }

    func onTapGestureRecognizer() {
        if emailTextField.text == "Ereeny@moweex.com" && passwordTextField.text == "1234" {
            let navVc = UINavigationController()
            let moviesViewController = MoviesViewController()
            navVc.viewControllers = [moviesViewController]
            UIApplication.shared.windows.first?.rootViewController = navVc
        }
    }

}
